<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tabbord_description' => 'Fait l’état des lieux : Taille Base, Espace Disque, listes Rubriques, Articles, Brèves...',
	'tabbord_nom' => 'Tableau de bord',
	'tabbord_slogan' => 'Fait l’état des lieux',
);

?>